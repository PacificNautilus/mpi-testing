#!/usr/bin/python

# import modules used here -- sys is a very standard one
from mpi4py import MPI
from datetime import datetime
import sys

def main():
    #Initialize Comm Object
    comm = MPI.COMM_WORLD
    #Set Rank
    rank = comm.Get_rank()
    #Set Size
    size = comm.Get_size()
    #Node Branch
    if rank == 0:
        #Master Only
        masterFxn(comm,rank,size, debug=False)
    else:
        #Workers
        workerFxn(comm,rank,size, debug=False)
    #Exit
    MPI.Finalize()
    sys.exit(0)
    

#Code Master Node Runs
def masterFxn(comm,rank, size, debug=False):
    #Master Code goes here
    if debug:
        print "Total size = " + str(size)
    #Set Maximum number to search to
    maxNumber = 1000
    #Let user know you are master
    if debug:
        print "I am the Master"
    #Initialize boolPrimeList
    boolPrimeList = [True] * maxNumber
    #Initialize multiplier (0 & 1 are not needed)
    currentMultiplier = 1
    #Continue until currentMultiplier is greater than maxNumber
    while currentMultiplier < maxNumber:
        #Initialize acutalSize to account for early termination
        #of currentMultiplier gerneration loop
        actualSize=1
        #Cycle through all ranks from 1 getting a multiplier and sending it to node
        for i in range(1,size):
            #grab the next valid multiplier
            currentMultiplier = getNextMultiplier(currentMultiplier, boolPrimeList, debug)
            #Check we haven't reached end of list
            if currentMultiplier < maxNumber:
                #Set data to send
                data = [currentMultiplier, maxNumber]
		#print data to be sent
		if debug:
			print "Sending Data to rank: " + str(i) +  " Data to Worker: " + str(data)
                #Send data to node
                comm.send(data,dest=i, tag=0)
                #set actualSize = i(current node)
                actualSize += 1
        #Cycle through all ranks from 1 getting data sent by nodes
        if debug:
           print "Master sent data to " + str(actualSize) + " workers"
        for i in range(1,actualSize):
            #Initialize multiples array
            multiples = []
            #Set multiples to data received from node
            if debug:
               print "Master waiting to recieve from worker " + str(i)
            multiples = comm.recv(source=i,tag=0)
	    #Print multiples received
	    if debug:
		print "Data Received from: " + str(i) +  " Data from Worker: " + str(multiples)
            #remove multiples from boolPrimeList
            boolPrimeList = removeMultiples(multiples,boolPrimeList)
    #Generating Primes complete need to convert from boolPrimeList to list of values
    #Set primesList
    primesList= getPrimes(boolPrimeList)
    print primesList
    kill_workers(comm, size, debug)

def kill_workers(comm, size, debug):
    for i in range(1,size):
        if debug:
            print "Sending Killsig  to rank: " + str(i)
                #Send the kill signal
        comm.send(-1,dest=i, tag=0)

#Code Worker Node Runs
def workerFxn(comm, rank, size, debug=False):
    while(True):
        #Worker Code goes here
        #Let user know which worker you are
        if debug:
            print "I am worker: " + str(rank)
        #Grab data from Master
        data = comm.recv(source=0, tag=0)
        if (data == -1):
            # if we get the kill signal, die
            sys.exit(0)
        if debug:
            print "Data " + "Worker: " + str(rank) + " Received from Master: " + str(data)
        #Set Multiplier value
        multiplier = data[0]
        #Set maxNumber value
        maxNumber = data[1]
        #Get multiples
        multiples = generateMultiples(multiplier,maxNumber)
        #print data to send
        if debug:
            print "Rank: " + str(rank) + " Sending to Master: " + str(multiples)
        #Send multiples to master
        comm.send(multiples,dest=0,tag=0)

#Function to get next valid prime number from boolPrimeList
def getNextMultiplier(currentMultiplier, boolPrimeList, debug):
    if debug:
        print (currentMultiplier, boolPrimeList)
    counter = currentMultiplier + 1
    while counter < len(boolPrimeList):
	if debug:
	    print ("couunter = " + str(counter))
        if boolPrimeList[counter]:
            return counter
        counter+=1
    return len(boolPrimeList) + 1

#Generates all multiples of multiplier less than maxNumber
def generateMultiples(multiplier, maxNumber):
    multiples = []
    counter = 2
    while multiplier * counter < maxNumber:
        multiples.append(multiplier*counter)
        counter += 1
    return multiples  

#Removes multiples from boolPrimeList
def removeMultiples(multiples,boolPrimeList):
    for multiple in multiples:
        if multiple < len(boolPrimeList):
            boolPrimeList[multiple] = False
    return boolPrimeList

#Returns list of indeces of all true values in boolPrimeList
def getPrimes(boolPrimeList):
    primes = []
    for i in range(1,len(boolPrimeList)):
        if boolPrimeList[i]:
            primes.append(i)
    return primes


# Standard boilerplate to call the main() function to begin
# the program.
if __name__ == '__main__':
    main()
    sys.exit(0)

# vim: ai ts=4 sts=4 et sw=4 ft=perl
# vim: autoindent tabstop=4 shiftwidth=4 expandtab softtabstop=4 filetype
