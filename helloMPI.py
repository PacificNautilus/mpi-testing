#Imports
from mpi4py import MPI

#main
def main():
	#Initialize MPI
	comm = MPI.COMM_WORLD
	#Get Rank
	rank = comm.Get_rank()
	#Get Size
	size = comm.Get_size()
	#Print them
	print "Rank: " + str(rank)
	print "Size: " + str(size)
	#Master Only
	if rank == 0:
		print "Hello from the master"

#Boilerplate
if __name__ == '__main__':
	main()
