#Imports
import sys, subprocess
from mpi4py import MPI

#main
def main():
	#Initialize MPI
	comm = MPI.COMM_WORLD
	#Get Rank
	rank = comm.Get_rank()
	#Get Size
	size = comm.Get_size()
	#Master Only
	if rank == 0:
		masterFunction(comm,rank,size)
	#Workers only
	else:
		workerFunction(comm,rank,size)

def masterFunction(comm, rank, size):
	print "Hello From Master"
	for i in range(1,size):
		data = [1*i,2*i,3*i,4*i]
		comm.send(data,dest=i,tag=0)

def workerFunction(comm, rank, size):
	message = comm.recv(source=0,tag=0)
	print str(rank) + " Received: " + str(message)


#Boilerplate
if __name__ == '__main__':
	main()
	sys.exit(0)
